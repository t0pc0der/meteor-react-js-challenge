import styled, {css} from 'styled-components';

export const StyledLi = styled.li`
    font-weight: bold;

    ${props =>
      props.className==='checked' &&
      css`
        text-decoration: line-through;
        font-weight: normal;
      `};
`;
