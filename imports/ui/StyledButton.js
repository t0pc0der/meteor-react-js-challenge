import styled from 'styled-components';

export const StyledButton = styled.button`
    color: tomato;
    float: right;
    font-weight: bold;
    background: none;
    font-size: 1.3em;
    border: none;
    position: relative;
  `;
